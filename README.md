# CefLib

Libreria per estendere le funzionalità di [CefSharp](https://cefsharp.github.io/)


Per far funzionare questa libreria occorre selezionare la configurazione di compilazione:

* x86
* x64

( AnyCPU non FUNZIONA!! )  

I file Release32 e Release64 vengono inseriti nell'eseguibile in base alla piattaforma selezionata:  

* Release32 -> x86
* Release64 -> x64

( impostazione fatta a mano sul file CSPROJ -> condition )  



Se ci sono problemi di recupero dei pacchetti nuget, provare una delle seguenti opzioni:  

* aprire il progetto come soluzione separata ed aprire il pannello dei paccheti NuGet -> se in alto compare una barra gialla che avvisa del problema, cliccare su ripristina  
* aprire il progetto come soluzione separata ed aprire il pannello dei paccheti NuGet -> disistallare e re-installare tutti i pacchetti  
* pulire il contenuto della cartella "packages" presente nella root del progetto e rieseguire le opzioni sopra  
* prega satana  
  
  
Per far funzionare ChromeBrowserControl nella visualizzazione grafica, occorre impostare la compilazione a x86