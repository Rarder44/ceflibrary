﻿using CefLib.Interfaces;
using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefLib.Controls
{
    public class BrowserLoadStartEventArgs
    {
        public TransitionType transitionType { get; set; }
        public string URL { get; set; }
        public BrowserLoadStartEventArgs(TransitionType transitionType, string URL)
        {
            this.transitionType = transitionType;
            this.URL = URL;
        }
    }
    public class JSobjectExistingException : Exception
    {
        public IBoundableObject obj { get; set; }
        public JSobjectExistingException(IBoundableObject obj)
        {
            this.obj = obj;
        }
        public override string Message
        {
            get
            {
                return "Il nome \"" + obj.BoundName + "\" è già associato ad un altro oggetto.";
            }
        }
    }
    public class ScriptNotRegisterException : Exception
    {
        public IScriptExecuter obj { get; set; }
        public ScriptNotRegisterException(IScriptExecuter obj)
        {
            this.obj = obj;
        }
        public override string Message
        {
            get
            {
                return "Lo script \"" + obj + "\" non è stato registrato in nessun browser.";
            }
        }
    }
    public class JSException : Exception
    {
        public String Script { get; set; }
        private String JSMessage { get; set; }
        public JSException(String Script, string Message)
        {
            this.Script = Script;
            this.JSMessage = Message;
        }
        public override string Message
        {
            get
            {
                return JSMessage;
            }
        }
    }
}
