﻿using CefLib.Classes;
using CefSharp.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CefLib.Controls
{
    /// <summary>
    /// Logica di interazione per UserControl1.xaml
    /// </summary>
    public partial class ChromeBrowserControl_WPF : UserControl, IChromeBrowserControl
    {
        ChromeBrowser cb;                   //Browser principale
        ChromiumWebBrowser _baseBrowser;    //Controller grafico associato al Browser cb



        public ChromeBrowser BaseBrowser => cb;
        public String Document => cb.Document;
        public String Url => cb.Url;



        public ChromeBrowserControl_WPF()
        {
            InitializeComponent();
            //TODO: Controllo se non sono nella modalità di disegno di visual studio

            //Se sono nella modalità di disegno, imposto un testo ( da problemi la visualizzazione del CEF in DesignTime
            if (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime)
            {
                TextBox t = new TextBox();
                t.Text = "ChromeBrowserControl\r\n\r\nIl componente verrà caricato in fase di esecuzione";
                dockPanel.Children.Add(t);
            }
            else
            {
                //Altrimenti aggiungo in controller ChromeBrowser
                cb = new ChromeBrowser_WPF(true);
                _baseBrowser = ((ChromeBrowser_WPF)cb).BrowserControl;
                _baseBrowser.Width = dockPanel.Width;
                _baseBrowser.Height = dockPanel.Height;
                dockPanel.Children.Add(_baseBrowser);

                cb.DocumentCompleted += Cb_DocumentCompleted;
                cb.DocumentStartLoad += Cb_DocumentStartLoad;
            }
        }

        public event EventHandler DocumentCompleted;
        public event EventHandler<BrowserLoadStartEventArgs> DocumentStartLoad;

        private void Cb_DocumentStartLoad(object sender, BrowserLoadStartEventArgs e)
        {
            DocumentStartLoad?.Invoke(sender, e);
        }
        private void Cb_DocumentCompleted(object sender, EventArgs e)
        {
            DocumentCompleted?.Invoke(sender, e);
        }






        public void NavigateInvoke(String Url)
        {
            if (!CheckAccess())
                this.Dispatcher.Invoke(()=> { NavigateInvoke(Url); });
            else
                Navigate(Url);
        }
        async public Task Navigate(String Url)
        {
            await cb.Navigate(Url);
        }
        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() IN UN THREAD SEPARATO!) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        async public Task NavigateAndWait(String Url)
        {
            await cb.NavigateAndWait(Url);
        }
        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() IN UN THREAD SEPARATO!) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        async public Task NavigateAndWait(String Url, Dictionary<String, String> PostData)
        {
            await cb.NavigateAndWait(Url, PostData);
        }



        /// <summary>
        /// Permette di fare l'inject di codice JS all'interno della pagina 
        /// </summary>
        /// <param name="javascript"></param>
        async public Task JS_Inject(string javascript)
        {
            await cb.JS_Inject(javascript);
        }
        /// <summary>
        /// Passare alla funzione del codice JS da eseguire, la funzione dovrà mettere il dato da restituire.....
        /// </summary>
        /// <param name="javascript"></param>
        /// <returns></returns>
        async public Task<Object> JS_Result(string javascript)
        {
            JSResponse x = await cb.JS_Result(javascript);
            return x.Result;
        }

    }
}
