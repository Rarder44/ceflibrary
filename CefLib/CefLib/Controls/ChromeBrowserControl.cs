﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using System.Runtime.CompilerServices;
using CefLib;
using System.IO;
//using CsQuery;
using static CefLib.Classes.ChromeBrowser;
using CefLib.Classes;
using System.Reflection;
using CefSharp.WinForms;

namespace CefLib.Controls
{
    public partial class ChromeBrowserControl : UserControl,IChromeBrowserControl
    {
        ChromeBrowser cb;                   //Browser principale
        ChromiumWebBrowser _baseBrowser;    //Controller grafico associato al Browser cb



        public ChromeBrowser BaseBrowser => cb; 
        public String Document => cb.Document;
        public String Url => cb.Url;




        public ChromeBrowserControl()
        {
            InitializeComponent();
            //TODO: Controllo se non sono nella modalità di disegno di visual studio

            //Se sono nella modalità di disegno, imposto un testo ( da problemi la visualizzazione del CEF in DesignTime
            if (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime) 
            {
                TextBox t = new TextBox();
                t.Dock = DockStyle.Fill;
                t.Multiline = true;
                t.Text = "ChromeBrowserControl\r\n\r\nIl componente verrà caricato in fase di esecuzione";
                this.Controls.Add(t);
            }
            else
            {
                //Altrimenti aggiungo in controller ChromeBrowser
                cb = new ChromeBrowser_WF(true);
                _baseBrowser = ((ChromeBrowser_WF)cb).BrowserControl;
                _baseBrowser.Dock = DockStyle.Fill;
                this.Controls.Add(_baseBrowser);

                cb.DocumentCompleted += Cb_DocumentCompleted;
                cb.DocumentStartLoad += Cb_DocumentStartLoad;
            }
        }


        public event EventHandler DocumentCompleted;
        public event EventHandler<BrowserLoadStartEventArgs> DocumentStartLoad;

        private void Cb_DocumentStartLoad(object sender, BrowserLoadStartEventArgs e)
        {
            DocumentStartLoad?.Invoke(sender, e);
        }
        private void Cb_DocumentCompleted(object sender, EventArgs e)
        {
            DocumentCompleted?.Invoke(sender,e);
        }



        public void NavigateInvoke(String Url)
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate { NavigateInvoke(Url); });
            else
                Navigate(Url);
        }
        async public Task Navigate(String Url)
        {   
            await cb.Navigate(Url); 
        }
        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() IN UN THREAD SEPARATO!) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        async public Task NavigateAndWait(String Url)
        {
            await cb.NavigateAndWait(Url);
        }
        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() IN UN THREAD SEPARATO!) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        async public Task NavigateAndWait(String Url, Dictionary<String, String> PostData)
        {
            await cb.NavigateAndWait(Url, PostData);
        }



        /// <summary>
        /// Permette di fare l'inject di codice JS all'interno della pagina 
        /// </summary>
        /// <param name="javascript"></param>
        async public Task JS_Inject(string javascript)
        {
            await cb.JS_Inject(javascript);
        }
        /// <summary>
        /// Passare alla funzione del codice JS da eseguire, la funzione dovrà mettere il dato da restituire.....
        /// </summary>
        /// <param name="javascript"></param>
        /// <returns></returns>
        async public Task<Object> JS_Result(string javascript)
        {
            JSResponse x= await cb.JS_Result(javascript);
            return x.Result;
        }

  







        /*public static void Shutdown()
        {
            ChromeBrowser.Shutdown();
        }*/
    }
}
