﻿using CefLib.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefLib.Controls
{
    public interface IChromeBrowserControl
    {
        event EventHandler DocumentCompleted;
        event EventHandler<BrowserLoadStartEventArgs> DocumentStartLoad;


        ChromeBrowser BaseBrowser { get; }
        String Document { get; }
        String Url { get; }



        void NavigateInvoke(String Url);
        Task Navigate(String Url);
        Task NavigateAndWait(String Url);
        Task NavigateAndWait(String Url, Dictionary<String, String> PostData);
        Task JS_Inject(string javascript);
        Task<Object> JS_Result(string javascript);



    }
}
