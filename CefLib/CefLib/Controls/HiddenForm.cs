﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefLib.Controls
{
    public partial class HiddenForm : Form
    {
        public HiddenForm()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Minimized;
        }


        protected override void OnClientSizeChanged(EventArgs e)
        {
            if (this.WindowState != FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Minimized;
            }
            base.OnClientSizeChanged(e);
        }
    }
}
