﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CefLib.Controls
{
    /// <summary>
    /// Logica di interazione per Window1.xaml
    /// </summary>
    public partial class HiddenWindowWPF : Window
    {
        public HiddenWindowWPF()
        {
            InitializeComponent();
            this.WindowState = WindowState.Minimized;
        }


        public void AddControlsInvoke(Control c)
        {
            Invoke(this, () => { grid.Children.Add(c); });
        }
        public void RemoveControlsInvoke(Control c)
        {
            Invoke(this, () => { grid.Children.Remove(c); });
        }
        public void Invoke(Control c,Action d)
        {
            if (!c.CheckAccess())
            {
                c.Dispatcher.Invoke(d);
            }
            else
                d();
        }
    }
}
