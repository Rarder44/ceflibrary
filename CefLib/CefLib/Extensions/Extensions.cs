﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefLib.Extensions
{
    static class Extensions
    {
        public static void AddControlsInvoke(this Control c, Control Child)
        {
            if (c.InvokeRequired)
                c.BeginInvoke((MethodInvoker)delegate { c.AddControlsInvoke(Child); });
            else
                c.Controls.Add(Child);
        }
        public static void RemoveControlsInvoke(this Control c, Control Child)
        {
            if (c.InvokeRequired)
                c.BeginInvoke((MethodInvoker)delegate { c.RemoveControlsInvoke(Child); });
            else
                c.Controls.Remove(Child);
        }

        public static void DisposeInvoke(this Control c)
        {
            if (c.InvokeRequired)
                c.BeginInvoke((MethodInvoker)delegate { c.DisposeInvoke(); });
            else
                c.Dispose();
        }
        public static void DisposeInvoke(this System.Windows.Controls.Control c)
        {
            if (c is IDisposable)
            {
                if (!c.CheckAccess())
                    c.Dispatcher.BeginInvoke((MethodInvoker)delegate { c.DisposeInvoke(); });
                else
                    ((IDisposable)c).Dispose();
            }
        }
    }
}
