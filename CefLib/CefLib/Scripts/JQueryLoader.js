//LA VARIABILE KEY VIENE IMPOSTATA DAL CODICE DI INJECTING PER GESTIRE IL VALORE DI RITORNO!!
var key = '%%KEY%%';
(async function () {
    await JQ_inject();
})()



async function JQ_inject() {
    if (window.jQuery) {
        //Gia caricato
        window.boundAsync.response(key, '1');

    } else {
        // jQuery  non caricato

        var Promessa = new Promise(resolve => {


            var script = document.createElement("SCRIPT");
            script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
            script.type = 'text/javascript';
            script.onload = async function () {
                window.boundAsync.response(key, '1');

            };
            document.getElementsByTagName("head")[0].appendChild(script);


        });

        return Promessa;
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


