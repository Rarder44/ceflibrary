﻿using CefLib.Classes;
using CefLib.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

//[assembly: InternalsVisibleTo("CefLib")]
namespace CefLib.Interfaces
{
    public abstract class IScriptExecuter
    {
        public ChromeBrowser browser { get; internal set; }

        public bool IsRegistered()
        {
            return (browser != null);                
        }
    }
}
