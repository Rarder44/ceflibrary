﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefLib.Interfaces
{
    public abstract class IBoundableObject
    {

        /// <summary>
        /// Nome del bound che verrà assegnato nel JS ( window."BoundName" )
        /// </summary>
        public String BoundName { get; set; }
        
        /// <summary>
        /// Permette di creare lo script per il bound con CEF
        /// </summary>
        /// <returns></returns>
        public String GetScript()
        {
            String script = Util.GetScript("CefLib.Scripts.Bound.js");
            script=script.Replace("%%NAME%%", BoundName);
            return script;
        }
    }
}
