﻿using CefLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefLib.Classes
{
    /// <summary>
    /// Oggetto per gestire la comunicazione con il JS ( visto che le risposte con l'await non funzionano molto bene... )
    /// </summary>
    public class GeneralResponseBound : IBoundableObject   //TODO: cambio nome e lo sposto
    {
        //Il dizionario contiene tutte le richieste in sospeso ( ognuna con una key generata dal metodo GenerateResponseRequest())
        Dictionary<string, string> awaitingResponse = new Dictionary<string, string>();

        public GeneralResponseBound(String BoundName)
        {
            this.BoundName = BoundName;
        }
        //ATTENZIONE!!!! nel codice JS bisogna scrivere la prima lettera MINUSCOLA! ( anche se nella funzione in c# la lettera è maiuscola!!!! )
        /// <summary>
        /// NON USARE QUESTA FUNZIONE!! questa funzione deve essere richiamata dal JS ( con la prima lettera in minuscolo! )
        /// </summary>
        /// <param name="key"></param>
        /// <param name="msg"></param>
        async public void Response(string key, string msg)
        {
            if (awaitingResponse.ContainsKey(key))
            {
                awaitingResponse[key] = msg;
            }
        }

        /// <summary>
        /// Permette di aspettare in maniera asincrona la risposta ad una richiesta 
        /// </summary>
        /// <param name="key">chiave della richiesta</param>
        /// <param name="timeout">tempo di attesa prima dello scadere della richiesta ( default 30 sec ) </param>
        /// <returns></returns>
        async public Task<string> AwaitResponse(string key, TimeSpan? timeout = null)
        {
            if (!awaitingResponse.ContainsKey(key))
            {
                return null;            //chiave non trovata... errore...
            }

            DateTime end;
            if (!timeout.HasValue)    //se non viene specificato nessun timeout, di default imposto 30 secondi
            {
                end = DateTime.Now + TimeSpan.FromSeconds(30);
            }
            else //se c'è impostato il timeout, aspetto fino al timeout
            {
                end = DateTime.Now + timeout.Value;
            }

            while (DateTime.Now < end)
            {
                if (awaitingResponse[key] != null) //se trovo il dato ( != null ) 
                {
                    //lo rimuovo dal dizionario e lo ritorno
                    string data = awaitingResponse[key];
                    awaitingResponse.Remove(key);
                    return data;
                }
                await Task.Delay(10);   //altrimenti aspetto 10 millisec e riprovo
            }

            return null;

        }

        /// <summary>
        /// Permette di generare una nuova richiesta e ne ritorna la key
        /// </summary>
        /// <returns></returns>
        async public Task<string> GenerateResponseRequest()
        {
            string key = "";
            do
            {
                Int64 unixTimestamp = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                key = Util.MD5(unixTimestamp + "");
                await Task.Delay(1);
            }
            while (awaitingResponse.ContainsKey(key));

            awaitingResponse[key] = null;
            return key;
        }
    }
}
