﻿using CefLib.Extensions;
using CefLib.Interfaces;
using CefLib.Controls;
using CefSharp;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CefLib.Classes
{
    public abstract class ChromeBrowser:IDisposable
    {
        //Descrive lo stato del browser -> statico in quanto CEF è una classe statica
        protected static bool inizializzato = false;
        //Usato per far funzionare CEF senza avere un form visibile ( CEF non funziona se non è legato ad un form... ) 
        //private static HiddenForm hf = null;

        //Browser Chromium Base
        //private ChromiumWebBrowser _wb;
        //Usato per caricare dell'HTML
        protected const String UrlToLoadHTML = "http://example/";
       

        /// <summary>
        /// Bound generico usabile per lo scambio di semplici messaggi
        /// </summary>
        public GeneralResponseBound globaBound { get; set; } = new GeneralResponseBound("boundAsync");



        public virtual  String Document { get; }

        public virtual  String Url { get; }




        #region Event

        public event EventHandler DocumentCompleted;
        public event EventHandler<BrowserLoadStartEventArgs> DocumentStartLoad;
        public event EventHandler IsBrowserInitializedChanged; 
        public event EventHandler<object> JavascriptMessageReceived;
        /// <summary>
        /// Ritornare TRUE per bloocare l'apertura del popup
        /// </summary>
        public event Func<string, bool> OnPopupRequest;


        protected void DocumentCompletedRiser(object sender,EventArgs e)
        {
            DocumentCompleted?.Invoke(sender, e);
        }
        protected void DocumentStartLoadRiser(object sender, BrowserLoadStartEventArgs e)
        {
            DocumentStartLoad?.Invoke(sender, e);
        }
        protected void IsBrowserInitializedChangedRiser(object sender, EventArgs e)
        {
            IsBrowserInitializedChanged?.Invoke(sender, e);
        }
        protected void JavascriptMessageReceivedRiser(object sender, JavascriptMessageReceivedEventArgs e)
        {
            JavascriptMessageReceived?.Invoke(sender, e.Message);
        }
        protected bool? OnPopupRequestRiser(String args)
        {
            return OnPopupRequest?.Invoke(args);
        }


        #endregion


        protected ChromeBrowser()
        {
            InitializeChromium();
        }

       

        private static void InitializeChromium()
        { 

            if (!inizializzato)
            {
                CefSettings settings = new CefSettings();
                settings.Locale = "it";
                settings.AcceptLanguageList = "it,it-IT";
                settings.CachePath =  Path.GetFullPath("cache");
                
                // Initialize cef with the provided settings
                //settings.CefCommandLineArgs.Add("debug-plugin-loading", "1");
                //settings.CefCommandLineArgs.Add("enable-logging", "1");

                //TODO: rimuovere!!
                settings.RemoteDebuggingPort = 8088;
                settings.LogSeverity = LogSeverity.Error;
                Cef.Initialize(settings);

                inizializzato = true;
            }

        }




        private void _wb_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            //DocumentCompleted?.Invoke(this,e);        
        }
        private void _wb_FrameLoadStart(object sender, FrameLoadStartEventArgs e)
        { 
            DocumentStartLoad?.Invoke(this,new BrowserLoadStartEventArgs(e.TransitionType,e.Url));
        }
        async private void _wb_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
         {
             if (!e.IsLoading)
             {
                await InitializeGlobalBound(true);  //permette di ricaricare il global bound sulla pagina nuova
                DocumentCompleted?.Invoke(this,e);
             }
         }
        private bool L_popup_request(string arg)
        {
            bool? b=OnPopupRequest?.Invoke(arg);
            if (b != null && b.HasValue)
                return b.Value;
            return false;
        }







        public abstract void LoadHtml(String HTML);

        /// <summary>
        /// Aspetta in manieara asincrona che il browser sia caricato ( richiamare con await per le funzioni grafiche ) 
        /// </summary>
        /// <returns></returns>
        abstract public Task WaitBrowserInitialize();


        /// <summary>
        /// Lancia il comando di navigazione asincrono e NON aspetta il caricamento della pagina
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        abstract public Task Navigate(String Url);

        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() ) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        abstract public Task NavigateAndWait(String Url);


        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() ) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        abstract public Task NavigateAndWait(String Url, Dictionary<String, String> PostData);


        /// <summary>
        /// Permette di fare l'inject di codice JS all'interno della pagina 
        /// </summary>
        /// <param name="javascript"></param>
        abstract public Task JS_Inject(string javascript);
        /// <summary>
        /// Passare alla funzione del codice JS da eseguire, la funzione dovrà mettere il dato da restituire.....
        /// </summary>
        /// <param name="javascript"></param>
        /// <returns></returns>
        abstract public Task<JSResponse> JS_Result(string javascript);
        /// <summary>
        /// Permette di eseguire uno script
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>



        /// <summary>
        /// Permette di inizializzare l'oggetto di comunicazione globale all'interno del JS ( vedere classe BoundObject ) 
        /// </summary>
        /// <returns></returns>
        public abstract Task InitializeGlobalBound(bool force = false);
        /// <summary>
        /// Permette di eseguire il Bound ad un oggetto
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public abstract Task RegisterJSObject(IBoundableObject obj, bool ForceReloadJS = false);
        
        /// <summary>
        /// Registra lo script all'interno del browser corrente
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        async public Task RegistScript(IScriptExecuter script)
        {
            script.browser = this;
        }

        /// <summary>
        /// Rimuove uno script dal browser corrente
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        async public Task UnRegistScript(IScriptExecuter script)
        {
            script.browser = null;
        }


        public static void Shutdown()
        {
            Cef.Shutdown();
            inizializzato = false;
        }
        public abstract void Dispose();


 
    }




   



  
}
