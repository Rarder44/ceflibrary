﻿using CefLib.Extensions;
using CefLib.Interfaces;
using CefLib.Controls;
using CefSharp;
using CefSharp.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CefLib.Classes
{
    public class ChromeBrowser_WPF:ChromeBrowser
    {

        //Usato per far funzionare CEF senza avere un form visibile ( CEF non funziona se non è legato ad un form... ) 
        private static HiddenWindowWPF hf = null;
        //Browser Chromium Base
        public ChromiumWebBrowser BrowserControl{ get; private set; }


        public override String Document
        {
            //TODO: CONTROLLO CHE IL task.Wait(); va a bloccare il codice!!!
            get
            {
                var task = BrowserControl.GetSourceAsync();
                task.Wait();
                return task.Result;

                //var task = _wb.EvaluateScriptAsync("document.getElementsByTagName ('html')[0].innerHTML");
                //task.Wait();
                //JavascriptResponse response = task.Result;
                //String EvaluateJavaScriptResult = response.Success ? ((string)response.Result ?? "null") : response.Message;
                //return EvaluateJavaScriptResult;
            }
        }
        public override String Url
        {
            get
            {
                if (!BrowserControl.CheckAccess())
                {
                    return BrowserControl.Dispatcher.Invoke<string>(() => { return this.Url; });
                }
                else
                    return BrowserControl.Address;
            }
        }
        









        public ChromeBrowser_WPF(bool HasParent=false)
        {
            InitializeChromium();

            //Create a browser component
            BrowserControl = new ChromiumWebBrowser("");
            if(!HasParent)
            {
                if(hf==null)
                {
                    hf = new HiddenWindowWPF();
                    hf.Show();
                }
                hf.AddControlsInvoke(BrowserControl);

            }
            

            BrowserControl.IsBrowserInitializedChanged += (object sender, System.Windows.DependencyPropertyChangedEventArgs e) => { this.IsBrowserInitializedChangedRiser(sender, null); };
            BrowserControl.FrameLoadEnd += _wb_FrameLoadEnd;
            BrowserControl.FrameLoadStart += _wb_FrameLoadStart;
            BrowserControl.JavascriptMessageReceived += _wb_JavascriptMessageReceived;
            LifespanHandler l = new LifespanHandler();
            l.popup_request += L_popup_request;
            BrowserControl.LifeSpanHandler = l;
            
            BrowserControl.LoadingStateChanged += _wb_LoadingStateChanged;       //non so xke è commentato!

        }

       

        private static void InitializeChromium()
        { 
            
            

            if (!inizializzato)
            {
                CefSettings settings = new CefSettings();
                // Initialize cef with the provided settings
                //settings.CefCommandLineArgs.Add("debug-plugin-loading", "1");
                //settings.CefCommandLineArgs.Add("enable-logging", "1");

                //TODO: rimuovere!!
                settings.RemoteDebuggingPort = 8088;
                settings.LogSeverity = LogSeverity.Error;
                Cef.Initialize(settings);

                inizializzato = true;
            }

        }




        private void _wb_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            if (e.Frame.IsMain)
            {
                DocumentCompletedRiser(this, e);
            }
                
        }
        private void _wb_FrameLoadStart(object sender, FrameLoadStartEventArgs e)
        { 
            DocumentStartLoadRiser(this,new BrowserLoadStartEventArgs(e.TransitionType,e.Url));
        }
        private void _wb_JavascriptMessageReceived(object sender, JavascriptMessageReceivedEventArgs e)
        {
            JavascriptMessageReceivedRiser(sender, e);
        }

        async private void _wb_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
         {
             /*if (!e.IsLoading)
             {
                await InitializeGlobalBound(true);  //permette di ricaricare il global bound sulla pagina nuova
                DocumentCompletedRiser(this,e);
             }*/
         }
        private bool L_popup_request(string arg)
        {
            bool? b=OnPopupRequestRiser(arg);
            if (b != null && b.HasValue)
                return b.Value;
            return false;
        }







        public override void LoadHtml(String HTML)
        {
            BrowserControl.LoadHtml(HTML, UrlToLoadHTML);
        }

        /// <summary>
        /// Aspetta in manieara asincrona che il browser sia caricato ( richiamare con await per le funzioni grafiche ) 
        /// </summary>
        /// <returns></returns>
        async override public Task WaitBrowserInitialize()
        {
            while (!BrowserControl.IsBrowserInitialized)
            {
                await Task.Delay(100);
            }
        }
        /// <summary>
        /// Lancia il comando di navigazione asincrono e NON aspetta il caricamento della pagina
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        async override  public Task Navigate(String Url)
        {
            await WaitBrowserInitialize();
            BrowserControl.Stop();
            BrowserControl.Load(Url);
        }
        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() ) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        async override public Task NavigateAndWait(String Url)
        {
            EventHandler<LoadingStateChangedEventArgs> Func = null;
            TaskCompletionSource<bool> t = new TaskCompletionSource<bool>();
            
            Func = (object sender, LoadingStateChangedEventArgs e) =>
            {
                if (!e.IsLoading)
                {
                    BrowserControl.LoadingStateChanged -= Func;
                    t.TrySetResult(true);
                }
            };
            BrowserControl.LoadingStateChanged += Func;

            await Navigate(Url);
            
            await t.Task;

        }
        /// <summary>
        /// Permette di navigare ed aspettare che la pagina venga caricata, per poter aspettare occorre richiamare questa funzione con l'operatore await ( o recuperare l'oggetto di ritorno Task e chiamare il Task.Wait() ) 
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        async override public Task NavigateAndWait(String Url, Dictionary<String,String> PostData)
        {
            await WaitBrowserInitialize();


            String script = @"
                        var frm = document.createElement('form');
                        frm.setAttribute('method', 'POST');
                        frm.setAttribute('action', '"+ Url + @"');
                        frm.setAttribute('enctype','application/x-www-form-urlencoded');
                        ";

            int i = 0;
            foreach(KeyValuePair<String,String> kv in PostData)
            {
                script += @"
                        var obj" + i + @"= document.createElement('input');

                        obj" + i+ @".setAttribute('type', 'hidden');
                        obj" + i + @".setAttribute('name', '"+kv.Key.Replace("'","\\'")+@"');
                        obj" + i + @".setAttribute('value', '" + kv.Value.Replace("'", "\\'") + @"');

                        frm.appendChild(obj" + i + @"); 
                        ";

                i++;
            }
            script += @"
                    document.body.appendChild(frm);
                    frm.submit();";


            String HTML = "<html><body><script>"+script+"</script></body></html>";



            TaskCompletionSource<bool> t = new TaskCompletionSource<bool>();

            EventHandler<FrameLoadEndEventArgs> Func = null;

            Func = (object sender, FrameLoadEndEventArgs e) =>
            {
                if(e.Url!=UrlToLoadHTML)
                {
                    BrowserControl.FrameLoadEnd -= Func;
                    t.TrySetResult(true);
                }
            };
            BrowserControl.FrameLoadEnd += Func;



            BrowserControl.LoadHtml(HTML, UrlToLoadHTML, Encoding.UTF8);

            await t.Task;
        }
     

        /// <summary>
        /// Permette di fare l'inject di codice JS all'interno della pagina 
        /// </summary>
        /// <param name="javascript"></param>
        async override public Task JS_Inject(string javascript)
        {
            await BrowserControl.EvaluateScriptAsync(javascript);
        }
        /// <summary>
        /// Passare alla funzione del codice JS da eseguire, la funzione dovrà mettere il dato da restituire.....
        /// </summary>
        /// <param name="javascript"></param>
        /// <returns></returns>
        async override public Task<JSResponse> JS_Result(string javascript)
        {
            return new JSResponse(await BrowserControl.EvaluateScriptAsync(javascript));

        }
        /// <summary>
        /// Permette di eseguire uno script
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
       


        /// <summary>
        /// Permette di inizializzare l'oggetto di comunicazione globale all'interno del JS ( vedere classe BoundObject ) 
        /// </summary>
        /// <returns></returns>
        public override async Task InitializeGlobalBound(bool force=false)
        {
            //lo faccio solo se non è già registrato
            if (!BrowserControl.JavascriptObjectRepository.IsBound(globaBound.BoundName) || force )
                await RegisterJSObject(globaBound, force);
        }
        /// <summary>
        /// Permette di eseguire il Bound ad un oggetto
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override async Task RegisterJSObject(IBoundableObject obj,bool ForceReloadJS=false)
        {
            /* //CODICE DI TEST DEL BOUND
            _wb.JavascriptObjectRepository.ObjectBoundInJavascript += (sender, e) =>
            {
                var name = e.ObjectName;
                Console.WriteLine($"Object {e.ObjectName} was bound successfully.");
            };*/


            if (!BrowserControl.JavascriptObjectRepository.IsBound(obj.BoundName))
            {
                //Lego alla variabile "boundAsync" ( che verrà creata in window.boundAsync in JS ) l'oggetto, impostandolo come asincrono 
                BrowserControl.JavascriptObjectRepository.Register(obj.BoundName, obj, true);
                //Carico lo script per il buond della variabile lato JS
                await JS_Inject(obj.GetScript());

                //A questo punto il bound è completato         
            }
            else if( ForceReloadJS)
            {
                await JS_Inject(obj.GetScript());
            }
            else
                throw new JSobjectExistingException(obj);
        }
        

        public override  void Dispose()
        {
            if (hf != null)
                hf.RemoveControlsInvoke(BrowserControl);
            BrowserControl.DisposeInvoke();
        }

        
        
        public void SendKey(int code)
        {
            KeyEvent k = new KeyEvent();
            k.WindowsKeyCode = code;
            k.FocusOnEditableField = true;
            k.IsSystemKey = false;
            k.Type = KeyEventType.Char;
            BrowserControl.GetBrowser().GetHost().SendKeyEvent(k);
        }
    }




   



  
}
