﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefLib.Classes
{

    public class JSResponse
    {
        public JSResponse(JavascriptResponse jsr)
        {
            this.Message = jsr.Message;
            this.Success = jsr.Success;
            this.Result = jsr.Result;
        }

        //     Error message
        public string Message { get; set; }
        //     Was the javascript executed successfully
        public bool Success { get; set; }
        //     Javascript response
        public object Result { get; set; }
    }
}
