﻿using CefLib.Interfaces;
using CefLib.Classes;
using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefLib.Controls;

namespace CefLib.ScriptExecuters
{
    //TODO: la faccio statica??

    /// <summary>
    /// Classe per la gestione dello script JQuery
    /// </summary>
    public class JQuery : IScriptExecuter
    {
       

        /// <summary>
        /// Permette di includere JQuery nella pagina
        /// </summary>
        /// <returns></returns>
        public async Task Bind()
        {
            if (!IsRegistered())
                throw new ScriptNotRegisterException(this);

            await browser.InitializeGlobalBound();

            //Carico lo script per l'inject di JQuery
            String script = Util.GetScript("CefLib.Scripts.JQueryLoader.js");
            //Creo una key per la gestione della risposta
            string key = await browser.globaBound.GenerateResponseRequest();
            //ricerco il carattere jolly e lo sostituisco con la key
            script = script.Replace("%%KEY%%", key);
            //injecto il codice ed aspetto che la funzione principale venga risolta  | TODO: controllo che la funzione è stata eseguita correttamente?
            JSResponse js = await browser.JS_Result(script);
            //tramite la classe di comunicazione, aspetto la risposta alla key generata in precendenza
            string Response = await browser.globaBound.AwaitResponse(key);

            //Controllo la risposta... TODO
            if (Response == "1")
            {
                //OK
            }
            else
            {
                //Errore
                //TODO: lo gestisco? -> ci riprovo?| do un errore?| do un eccezione?| bestemmio? ... chi lo sa!
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="JQuerySelector"></param>
        /// <param name="PropertyName"></param>
        /// <param name="Value">Se è una stringa, ricordarsi di mettere le virgolette singole '' per racchiudere -> string s ="'Value'";</param>
        /// <returns></returns>
        public async Task SetProperty(string JQuerySelector, string PropertyName, string Value)
        {
            if (!IsRegistered())
                throw new ScriptNotRegisterException(this);

            string JS = "(function(){ return $('" + JQuerySelector + "').prop('" + PropertyName + "'," + Value + "); })();";
            JSResponse ret = await browser.JS_Result(JS);
            if (!ret.Success)
                throw new JSException(JS, ret.Message);
        }
        public async Task<T> GetProperty<T>(string JQuerySelector, string PropertyName)
        {
            if (!IsRegistered())
                throw new ScriptNotRegisterException(this);

            string JS = "(function(){ return $('" + JQuerySelector + "').prop('" + PropertyName + "'); })();";
            JSResponse ret = await browser.JS_Result(JS);
            if (!ret.Success)
                throw new JSException(JS, ret.Message);


            return (T)ret.Result;
        }
        public async Task<T> GetAttribute<T>(string JQuerySelector, string AttributeName)
        {
            if (!IsRegistered())
                throw new ScriptNotRegisterException(this);

            string JS = "(function(){ return $('" + JQuerySelector + "').attr('" + AttributeName + "'); })();";
            JSResponse ret = await browser.JS_Result(JS);
            if (!ret.Success)
                throw new JSException(JS, ret.Message);


            return (T)ret.Result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="JQuerySelector"></param>
        /// <param name="AttributeName"></param>
        /// <param name="Value">Se è una stringa, ricordarsi di mettere le virgolette singole '' per racchiudere -> string s ="'Value'";</param>
        /// <returns></returns>
        public async Task SetAttribute(string JQuerySelector, string AttributeName,string Value)
        {
            if (!IsRegistered())
                throw new ScriptNotRegisterException(this);

            string JS = "(function(){ return $('" + JQuerySelector + "').attr('" + AttributeName + "',"+ Value + "); })();";
            JSResponse ret = await browser.JS_Result(JS);
            if (!ret.Success)
                throw new JSException(JS, ret.Message);
        }




    }
}
