﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CefLib
{
    internal static class Util
    {

        internal static String GetScript(string ResourceScriptPath)
        {
            Assembly targetAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            Stream s =targetAssembly.GetManifestResourceStream(ResourceScriptPath);
            String text = "";
            using (StreamReader reader = new StreamReader(s))
            {
                text = reader.ReadToEnd();
            }
            return text;
        }
        internal static  string MD5(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

 

        internal static SystemBit GetSystemBit()
        {
            if (IntPtr.Size == 4)
                return SystemBit.x32;
            else 
                return SystemBit.x64;
        }


        private static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryName, bool overwrite)
        {
           
            /*
                archive.ExtractToDirectory(destinationDirectoryName, overwrite);
                return;*/
            

            foreach (ZipArchiveEntry file in archive.Entries)
            {
                string completeFileName = Path.Combine(destinationDirectoryName, file.FullName);
                if (file.Name == "")
                {// Assuming Empty for Directory
                    Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
                    continue;
                }

                try//Alcuni file possono esserci nella cartella e possono essere in esecuzione -> evito di dare eccezzione
                {
                    file.ExtractToFile(completeFileName, overwrite); 
                }
                catch (Exception e)
                {

                }
                
            }
        }



        internal static void ExtractCEFfile()
        {
            //Ottengo l'assembly corrente
            Assembly targetAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            
            //Estraggo i file specifici per la piattaforma x86/x64 
            Stream SRelease = null;
            SystemBit sb = GetSystemBit();
            if(sb==SystemBit.x32)
            {
                SRelease = targetAssembly.GetManifestResourceStream("CefLib.Redist.Release32.zip");
                
            }
            else if (sb == SystemBit.x64)
            {
                SRelease = targetAssembly.GetManifestResourceStream("CefLib.Redist.Release64.zip");

            }


            using (ZipArchive archive = new ZipArchive(SRelease))
            {
                archive.ExtractToDirectory("./", false);
            }
           

        }


    }
    internal enum SystemBit
    {
        x32,
        x64
    }
}
